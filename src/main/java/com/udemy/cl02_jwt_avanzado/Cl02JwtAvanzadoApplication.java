package com.udemy.cl02_jwt_avanzado;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter", "com.udemy.cl02_jwt_avanzado"})
public class Cl02JwtAvanzadoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Cl02JwtAvanzadoApplication.class, args);
    }

}
